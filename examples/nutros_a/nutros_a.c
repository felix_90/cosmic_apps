/****************************************************************************
 * examples/hello/hello_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <string.h>

#include <sys/boardctl.h>
#include <sys/ioctl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sched.h>
#include <errno.h>
#include <signal.h>
#include <semaphore.h>

#include <mqueue.h>


#include <nuttx/arch.h>

#include <nuttx/timers/timer.h>

#include <nuttx/usb/cdcacm.h>

#define CHILD_ARG ((void*)0x12345678)

#define MAGIC_BYTE_1 0x42
#define MAGIC_BYTE_2 0x8A


#define leng_ping 31

static char ping_message[31];
static char encoder_message[31];
static int flag = 0;
int ping_flag =1;
static int fd;

/*sems for transfer handeling*/
static sem_t sem_serial_in;
static sem_t sem_serial_out;
/* Message Typs*/

#if 0
static struct global_message{
	int id_type;
	int length_msg;
	char msg_256[249];
};
#endif

const char* msgq_ping  = "1";
const char* msgq_encoder  = "3";
const char* msgq_temp = "4";

mqd_t mqd_1;
mqd_t mqd_2;
mqd_t mqd_3;

/****************************************************************************
 * crc check
 ****************************************************************************/
uint16_t Crc16(uint16_t crc, uint8_t *buf, uint32_t len){
     //uint16_t crc=0x0000;
     static const uint16_t crc16_tab[256] =
     {

         0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
         0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
         0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
         0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
         0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
         0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
         0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
         0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
         0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
         0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
         0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
         0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
         0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
         0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
         0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
         0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
         0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
         0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
         0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
         0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
         0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
         0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
         0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
         0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
         0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
         0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
         0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
         0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
         0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
         0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
         0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
         0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
     };

     while (len--){
         crc = crc16_tab[(crc>>8) ^ *buf++] ^ (crc << 8);
     }
     return crc;
}


 static int nutros_serial_in_task (int argc, char *argv[]) //,int signo
 {
	 FAR struct timespec rec_time;
	 long ros_sec;

	 int stage;
	 int msg_counter;
	 int nbytes;
	 int msg_length;
	 int msg_inv_length;
	 char msg[251];
	 char magic_1;
	 char magic_2;
	 char len;
	 char len_inv;
	 char id;
	 char CRC[1];

	 char buffer[17];
	 //mqd_t mqd_1;
	 int ret;

	 usleep(100);
	do{
		usleep(100);
		//printf("serial in wartet auf sem \n");
		//sem_wait(&sem_serial_in);

		 //  printf("START READING \n");
	    do{
	    	//printf("START READING \n");
	   // printf("stage: %i \n", stage);
	    switch(stage){
	    	case 0:
	    		//printf("magic 1 \n");
	    		clock_gettime(1, &rec_time);
	    		nbytes = read(fd, &magic_1, 1);
	    		//printf("magic 1 gelesen\n");
	    		if(magic_1 == MAGIC_BYTE_1 ){
	    			stage ++;
	    		}else{
	    			stage = 0;
	    		}
	    		//printf("stage: %i \n", stage);
	    		break;
	    	case 1:
	    		//printf("magic 2 \n");
	    		nbytes = read(fd, &magic_2, 1);
	    		if(magic_2 == MAGIC_BYTE_2 ){
	    			stage ++;
	    		}else{
	    			stage = 0;
	    		}
	    		//printf("stage: %i \n", stage);
	    		break;
	    	case 2:
	    		//printf("len \n");
	    		nbytes = read(fd, &len, 1);
	    		//printf("len: %i \n", len);
	    		nbytes = read(fd, &len_inv, 1);
	    		//printf("len_inv: %i \n", len_inv);
	    		len_inv = 256-len_inv;
	    		if(len == len_inv){
	    			stage ++;
	    		}else{
	    			stage = 0;
	    		}
	    		//printf("stage: %i \n", stage);
	    		break;
	    	case 3:
	    		//printf("id \n");
	    		nbytes = read(fd, &id, 1);
	    		stage ++;
	    		//printf("stage: %i \n", stage);
	    		break;
	    	case 4:
	    		//printf("msg\n");
	    		msg_counter = 0;
	    		do{
	    			//printf("message counter %i \n", msg_counter);
	    			nbytes = read(fd, &msg[msg_counter], 1);
	    			//printf("message 0x%02x \n",msg[msg_counter]);

	    			msg_counter ++;
	    		}while(msg_counter <= (len-8));
	    		stage ++;
	    		//printf("stage: %i \n", stage);
	    		break;
	    	case 5:
	    		//printf("CRC\n");
	    		nbytes = read(fd, &CRC, 2);
	    		//printf("crc1 0x%02x \n",CRC[0]);
	    		//printf("crc2 0x%02x \n",CRC[1]);
	    		stage ++;
	    		//printf("stage: %i \n", stage);
	    		break;
	    }
	    }while(stage <= 5);

	    stage = 0;

		switch(id){
			case 0:

				/*Incoming MSG fin - get the first timestap*/
				clock_gettime(1, &rec_time);
				/*build the ping msg*/
				ping_message[0] = MAGIC_BYTE_1;
				ping_message[1] = MAGIC_BYTE_2;
				ping_message[2] = len;
				ping_message[3] = 256-len;
				ping_message[4] = id;
				ping_message[29] = 0xab; //CRC
				ping_message[30] = 0xcd; //CRC
				/*copy the ROS sec& nsec Timestap in ping_message*/
				ros_sec = *(long*)(&msg[0]);
				*(long*)(&ping_message[5])= ros_sec;
				ros_sec = *(long*)(&msg[4]);
				*(long*)(&ping_message[9])= ros_sec;
				/*copy the Nuttx Rx  Timestap in ping_message*/
				*(uint32_t *)(&ping_message[13])= rec_time.tv_sec;
				*(long*)(&ping_message[17])= rec_time.tv_nsec;

				 /*write 0x31 into buffer[0], need for flag*/
				 buffer[0] = '1';
				 /*send buffer to msgq ping*/
				 ret =  mq_send(mqd_1,buffer,1,1);

				 //mq_close(mqd_1);
				 flag = 0; /*temp*/
				 /*need to notice first ping*/
				 //printf("Ping gelesen \n");
				 ping_flag = 0;
			}

	  //pause();
	  //  sem_post(&sem_serial_out);
	}while(1);
	return 0;
 }

 static int nutros_serial_out_task (int argc, char *argv[]) //,int signo
  {
  	int ret;
  	FAR struct timespec tx_time;
  	struct timespec timeout={10,0};
  	char buffer[17];
  	//char buffer_2;
  	//mqd_t mqd_1;
  	int prio;


  	do{
  		/*do nothing wait for first ping*/
  		 printf(" Wait for ping \n");

  	}while(ping_flag);


  	do{
  		/*Write Ping, if ready*/
  		ret = mq_timedreceive(mqd_1,buffer,17,&prio,&timeout);

  		if(buffer[0]=='1'){

  			/*get timestap*/
  			clock_gettime(1, &tx_time);

  			*(uint32_t *)(&ping_message[21])= tx_time.tv_sec;
  			*(long*)(&ping_message[25])= tx_time.tv_nsec;

  			ret = write(fd,&ping_message,sizeof(ping_message));


  			//printf("Write usb buffer: %i \n",ret);
  			//printf("Ping end\n",buffer[0]);


  			buffer[0] = 0;


  			ret =  mq_send(mqd_1,buffer,1,1);
  			//printf("Ping Write \n",ret);

  		}else{

  			//printf("No ping msg to write\n",mqd_1);

  		}

  		buffer[0] =0;




  		/*check for Vamex Encoder*/


  		ret = mq_timedreceive(mqd_2,buffer,17,&prio,&timeout);
  		if(buffer[0]=='A'){

  			clock_gettime(1, &tx_time);
  			*(uint32_t *)(&encoder_message[5])= tx_time.tv_sec;

  			*(long*)(&encoder_message[9])= tx_time.tv_nsec;


  			encoder_message[0] = MAGIC_BYTE_1;
  			encoder_message[1] = MAGIC_BYTE_2;
  			encoder_message[2] = 31;
  			encoder_message[3] = 256-31;
  			encoder_message[4] = 50;


  			encoder_message[13] = buffer[1];
  			encoder_message[14] = buffer[2];
  			encoder_message[15] = buffer[3];
  			encoder_message[16] = buffer[4];


  			encoder_message[17] = buffer[5];
  			encoder_message[18] = buffer[6];
  			encoder_message[19] = buffer[7];
  			encoder_message[20] = buffer[8];


  			encoder_message[21] = buffer[9];
  			encoder_message[22] = buffer[10];
  			encoder_message[23] = buffer[11];
  			encoder_message[24] = buffer[12];


  			encoder_message[25] = buffer[13];
  			encoder_message[26] = buffer[14];
  			encoder_message[27] = buffer[15];
  			encoder_message[28] = buffer[16];

  			encoder_message[29] = 0xab; //CRC
  			encoder_message[30] = 0xcd;


  			write(fd,&encoder_message,sizeof(encoder_message));
  			//printf("Encoder\n");
  		}else{
  			//printf("No Encoder msg to write\n",mqd_1);
  		}

  		buffer[0] =0;

  		//check for Vamex Temp

  		ret = mq_timedreceive(mqd_3,buffer,17,&prio,&timeout);
 		if(buffer[0]=='C'){
 			clock_gettime(1, &tx_time);
 			*(uint32_t *)(&encoder_message[5])= tx_time.tv_sec;
 			*(long*)(&encoder_message[9])= tx_time.tv_nsec;

 			encoder_message[0] = MAGIC_BYTE_1;
 			encoder_message[1] = MAGIC_BYTE_2;
 			encoder_message[2] = 31;
 			encoder_message[3] = 256-31;
 			encoder_message[4] = 51;

 			encoder_message[13] = buffer[1];
 			encoder_message[14] = buffer[2];
 			encoder_message[15] = buffer[3];
 			encoder_message[16] = buffer[4];

 			encoder_message[17] = buffer[5];
 			encoder_message[18] = buffer[6];
 			encoder_message[19] = buffer[7];
 			encoder_message[20] = buffer[8];

 			encoder_message[21] = buffer[9];
 			encoder_message[22] = buffer[10];
 			encoder_message[23] = buffer[11];
 			encoder_message[24] = buffer[12];

 			encoder_message[25] = buffer[13];
 			encoder_message[26] = buffer[14];
 			encoder_message[27] = buffer[15];
 			encoder_message[28] = buffer[16];

 			encoder_message[29] = 0xab; //CRC
 			encoder_message[30] = 0xcd;
 			write(fd,&encoder_message,sizeof(encoder_message));
 			//printf("Temperatur\n",mqd_1);
 		}else{
 			//printf("No temp msg to write\n",mqd_1);
 		}

  		buffer[0] =0;

  		//sleep(2);
  		usleep(10);
  	}while(1);//testto<10

  	//close(fd);
  	return 0;
  }


#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int nutros_a_main(int argc, char *argv[])
#endif
{
//	char msg_test[249];

	struct boardioc_usbdev_ctrl_s ctrl;
	FAR void *handle;
	int ret;

    /*Create USB DEVICE */
    /* Initialize the USB serial driver */

	#if defined(CONFIG_PL2303) || defined(CONFIG_CDCACM)
	#ifdef CONFIG_CDCACM

    ctrl.usbdev   = BOARDIOC_USBDEV_CDCACM;;
    ctrl.action   = BOARDIOC_USBDEV_CONNECT;
    ctrl.instance = 0;
    ctrl.handle   = &handle;

	#else
    ctrl.usbdev   = BOARDIOC_USBDEV_PL2303;
    ctrl.action   = BOARDIOC_USBDEV_CONNECT;
    ctrl.instance = CONFIG_NSH_USBDEV_MINOR;
    ctrl.handle   = &handle;
 	#endif

    ret = boardctl(BOARDIOC_USBDEV_CONTROL, (uintptr_t)&ctrl);
    UNUSED(ret); /* Eliminate warning if not used */
    DEBUGASSERT(ret == OK);
   	#endif

	do{
		fd = open("/dev/ttyACM0", O_RDWR);
		if (fd < 0){
			DEBUGASSERT(errno == ENOTCONN);
			printf("USB not connected\n");
			sleep(1);
		}else{
			printf("USB connected\n");
		}
	}while (fd < 0);
	//close(fd);
	sem_init(&sem_serial_in,0,-1);
	sem_init(&sem_serial_out,0,-1);
	sem_post(&sem_serial_in);


	/*	Create msgq
	 *  1 - Ping Msg
	 *  3 - Vamex Encoder
	 *  4 - Vamex Temp
	 */


	 mqd_1 = mq_open(msgq_ping,O_CREAT,16,NULL); //O_RDWR
	 mqd_1 = mq_open(msgq_ping,O_RDWR);
	 if(mqd_1 < 0){
		 printf("msgq failed \n");
	 }else{
		 printf("msgq ping success: %i \n",mqd_1);
	 }
	 //mq_close(mqd_1);


	 mqd_2 = mq_open(msgq_encoder,O_CREAT,16,NULL); //O_RDWR
	 mqd_2 = mq_open(msgq_encoder,O_RDWR);
	 if(mqd_1 < 0){
		 printf("msgq failed \n");
	 }else{
		 printf("msgq encoder success: %i \n",mqd_2);
	 }
	 //mq_close(mqd_1);

	 mqd_3 = mq_open(msgq_temp,O_CREAT,16,NULL); //O_RDWR
	 mqd_3 = mq_open(msgq_temp,O_RDWR);
	 if(mqd_1 < 0){
		 printf("msgq failed \n");
	 }else{
		 printf("msgq temp success: %i \n",mqd_1);
	 }
	 //mq_close(mqd_1);

	ret = task_create("nutros_serial_out_task",2, 1024, nutros_serial_out_task, NULL);

    ret = task_create("nutros_serial_in_task", 2, 1024, nutros_serial_in_task, NULL);


  return 0;
}
