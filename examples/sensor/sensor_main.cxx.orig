/****************************************************************************
 * examples/sensor/sensor_main.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "lib/sensor/magnet.h"
#include "lib/sensor/pressure.h"
#include "lib/uavcan/uavcan.h"
#include <cmath>
#include <time.h>
#include <mqueue.h>
#include <fcntl.h>
#include <semaphore.h>


#define UAVCAN_START 	0x00
#define UAVCAN_SEND 		0x01
#define UAVCAN_STOP 		0x02

struct uavcanData_s{
	uint64_t timestamp;
	int32_t height_mm;
	int64_t longitude_1e8;
	int64_t latitude_1e8;
	float velocity[3];
	float yaw_degree;
	float pitch_degree;
	float roll_degree;
};

// Message Queue
static mqd_t queue;
static char* queue_name;

// UAVCAN Data
static uavcanData_s data;
static uavcan::Node* node;
static uavcan::gnss_aux_s aux;
static uavcan::gnss_fix_s fix;
static uavcan::magnet_s magnet;
static uavcan::pressure_s pressure;
static uavcan::temperature_s temperature;
static uavcan::node_status_s nodeStatus;
static timespec startTime;
static float inclination;
static float declination;
static bool started = false;
static sem_t *sem;


/****************************************************************************
 * init_app
 * Starts the uavcan node and sets all static data, that will not change
 * during operation.
 ****************************************************************************/
//int init_app() {
int init_app(int argc, FAR char *argv[]) {
	/*
	 * Open semaphore and wait until access is granted
	 */
	sem_t *semaStart;
	semaStart = sem_open("/uavcan", 0);
	sem_wait(sem);

	float pdop = sqrt(powf(CONFIG_EXAMPLES_SENSOR_GNSS_HDOP/100,2)
			+ powf(CONFIG_EXAMPLES_SENSOR_GNSS_VDOP/100,2));
	float gdop = sqrt(powf(pdop,2) + powf(CONFIG_EXAMPLES_SENSOR_GNSS_TDOP/100,2));
	// Assumes that the DOP in north and east is the same and calculates its
	// from the horicontal DOP
	float endop = sqrt(0.5*CONFIG_EXAMPLES_SENSOR_GNSS_HDOP/100
			*CONFIG_EXAMPLES_SENSOR_GNSS_HDOP/100);  //EDOP (East DOP) NDOP (North DOP)

	// Start node allocation
	node = uavcan::Node::getNode();
	if (1) {
		node->setNodeId(20);
	}
	node->start();

	//GNSS Auxiliary
	node->setPriority(uavcan::Auxiliary::getDatatype(), 20);
	aux.vdop = CONFIG_EXAMPLES_SENSOR_GNSS_VDOP/1000;
	aux.edop = endop;  	//East
	aux.gdop = gdop;
	aux.hdop = CONFIG_EXAMPLES_SENSOR_GNSS_HDOP/1000;
	aux.ndop = endop;	//North
	aux.pdop = pdop;
	aux.tdop = CONFIG_EXAMPLES_SENSOR_GNSS_TDOP/1000;
	aux.sats_used = CONFIG_EXAMPLES_SENSOR_GNSS_SATELITES;
	aux.sats_visible = CONFIG_EXAMPLES_SENSOR_GNSS_SATELITES;

	//GNSS Fix
	node->setPriority(uavcan::Fix::getDatatype(), 16);
		//TimeData
	fix.gnss_time_standard = uavcan::UTC;
	fix.num_leap_seconds = CONFIG_EXAMPLES_SENSOR_GNSS_LEAPSECONDS;
		//PositionData

	fix.positionCovariance[0] = CONFIG_EXAMPLES_SENSOR_GNSS_POSITION_VARIANCE/1000;
	fix.positionCovariance[1] = CONFIG_EXAMPLES_SENSOR_GNSS_POSITION_VARIANCE/1000;
	fix.positionCovariance[2] = CONFIG_EXAMPLES_SENSOR_GNSS_POSITION_VARIANCE/1000;
	fix.sizePositionCovariance = 3;
		//VelocityData

	fix.velocityCovariance[0] = CONFIG_EXAMPLES_SENSOR_GNSS_VELOCITY_VARIANCE/1000;
	fix.sizeVelocityCovariance = 1;
		//StatusData
	fix.sats_used = CONFIG_EXAMPLES_SENSOR_GNSS_SATELITES;
	fix.pdop = pdop;
	fix.status = uavcan::FIX_3D;

	//MagneticFieldStrength
	node->setPriority(uavcan::MagneticFieldStrength::getDatatype(), 16);
	magnet.covariance[0] = CONFIG_EXAMPLES_SENSOR_MAGNET_VARIANCE/100;
	inclination = CONFIG_EXAMPLES_SENSOR_MAGNET_INCLINATON;
	declination = CONFIG_EXAMPLES_SENSOR_MAGNET_DECLINATION;

	//StaticPressure
	node->setPriority(uavcan::StaticPressure::getDatatype(), 16);
	pressure.variance = CONFIG_EXAMPLES_SENSOR_PRESSURE_VARIANCE/100;

	//StaticTemperature
	node->setPriority(uavcan::StaticTemperature::getDatatype(), 16);
	temperature.temperature = CONFIG_EXAMPLES_SENSOR_TEMPERATURE_0;
	temperature.variance = CONFIG_EXAMPLES_SENSOR_TEMPERATURE_VARIANCE/100;



	node->setPriority(uavcan::StaticTemperature::getDatatype(), 20);
	nodeStatus.health = 0;
	nodeStatus.mode = 0;
	nodeStatus.subMode = 0;
	nodeStatus.vendorStatus = 0;
	nodeStatus.time = 0;
	clock_gettime(CLOCK_REALTIME, &startTime);
	started = true;
<<<<<<< HEAD
=======
	/*
	 * Release semaphore
	 */
	sem_post(semaStart);
	return 0;
>>>>>>> 8f8cb5e5201983f613807972ef9cc45a18d0a071
}

/****************************************************************************
 * send_app
 * Calculates and send the data, which is inside the data struct via CAN.
 ****************************************************************************/
int send_app(int argc, FAR char *argv[]) {
	/*
	 * Read data from queue, if there is no new data or an error, stop sending
	 */
	int ret = 0;
	ret = mq_receive(queue, (char *)&data, sizeof(data), 0);
	if (ret < 0) {
		return ret;
	}

	/*
	 * If the initialisation and starting process of the uavcan node is not
	 * finished, stop sending process
	 */
	if (!started) {
		return -1;
	}

	/*
	 * Try to access semaphore, if semaphore is blocked, another thread is
	 * currently trying to send data. Stop this sending process and therefore this
	 * thread, to avoid creating to many threads.
	 */
	sem_t *semaSend;
	semaSend = sem_open("/uavcan", 0);
	int retSem = 0;
	retSem = sem_trywait(semaSend);
	if (retSem < 0) {
		return retSem;
	}

	/*
	 * New Data is read from the message queue, the uavcan node is already
	 * initialised and started and no other thread is currently trying to
	 * send data via the uavcan bus. Start creating the messages and send
	 * them on the uavcan bus. Enter critical section.
	 */

	//MagneticFieldStrength
	float magneticField[3];
	Magnet::calcMagnet(magneticField, inclination, declination, data.yaw_degree,
			data.pitch_degree, data.roll_degree);
	magnet.x = magneticField[0];
	magnet.y = magneticField[1];
	magnet.z = magneticField[2];
	uavcan::MagneticFieldStrength magnetMsg = uavcan::MagneticFieldStrength(magnet);
	node->send(&magnetMsg);


	//StaticPressure
	pressure.pressure = Pressure::calcPressure(data.height_mm);
	uavcan::StaticPressure pressureMsg = uavcan::StaticPressure(pressure);
	node->send(&pressureMsg);

	//StaticTemperature
	uavcan::StaticTemperature temperatureMsg = uavcan::StaticTemperature(temperature);

	//GNSS Fix
	fix.gnss_timestamp = data.timestamp;
	fix.timestamp = fix.gnss_timestamp;
	fix.height_ellipsoid_mm = data.height_mm;
	fix.height_msl_mm = fix.height_ellipsoid_mm;
	fix.latitude_deg_1e8 = data.latitude_1e8;
	fix.longitude_deg_1e8 = data.longitude_1e8;
	fix.ned_velocity[0] = data.velocity[0];
	fix.ned_velocity[1] = data.velocity[1];
	fix.ned_velocity[2] = data.velocity[2];
	uavcan::Fix fixMsg = uavcan::Fix(fix);
	node->send(&fixMsg);

	timespec currentTime;
	clock_gettime(CLOCK_REALTIME, &currentTime);
	nodeStatus.time = (currentTime.tv_sec - startTime.tv_sec)
			+ ((currentTime.tv_nsec - startTime.tv_nsec)/1000000000);
	uavcan::NodeStatus nodeMsg = uavcan::NodeStatus(nodeStatus);
	node->send(&nodeMsg);

	//GNSS Auxiliary
	uavcan::Auxiliary auxMsg = uavcan::Auxiliary(aux);
	node->send(&auxMsg);

	/*
	 * Leave critical section. Release semaphore.
	 */
	sem_post(semaSend);

	return 0;

}

/****************************************************************************
 * stop_app
 * Stops the can node and deletes it. Sets the started flag to false.
 ****************************************************************************/
void stop_app() {
	uavcan::Node::getNode()->stop();
	node = NULL;
	started = false;
}

<<<<<<< HEAD
static int uavcan_task(int argc, char *argv[]) {
	timespec tp;
	clock_gettime(CLOCK_REALTIME, &tp);

	sensor.barometer = true;
	sensor.gps = true;
	sensor.magnetometer = true;
	sensor.temperature = true;

	data.height_mm = 100;
	data.latitude_1e8 = 1;
	data.longitude_1e8 = 1;
	data.pitch_degree = 1;
	data.roll_degree = 1;
	data.yaw_degree = 1;
	data.velocity[0] = 1;
	data.velocity[1] = 1;
	data.velocity[2] = 1;
	data.timestamp = tp.tv_sec*1000+tp.tv_nsec/1000000;
	init();
	while (started) {
		//random = (rand()/RAND_MAX)*10;
		//random = 0;

		clock_gettime(CLOCK_REALTIME, &tp);
		data.height_mm = 100.0;
		data.latitude_1e8 = 1.0;
		data.longitude_1e8 = 1.0;
		data.pitch_degree = 1;
		data.roll_degree = 1;
		data.yaw_degree = 1;
		data.velocity[0] = 1.0;
		data.velocity[1] = 1.0;
		data.velocity[2] = 1.0;
		data.timestamp = tp.tv_sec*1000+tp.tv_nsec/1000000;
		send();
		usleep(10000);
=======
/****************************************************************************
 * start_task
 * Starts the initialisation process in its own task. Return error code of
 * thread creation, if an error occurs, otherwise 0.
 ****************************************************************************/
int start_task() {
	int ret;
	ret = task_create("StartUAVAN", 19, 1024, init_app, NULL);
	if (ret < 0) {
		return ret;
	}
	return 0;
}


/****************************************************************************
 * send_task
 * Starts the sending process in its own task. Return error code of
 * thread creation, if an error occurs, otherwise 0.
 ****************************************************************************/
int send_task() {
	int ret;
	ret = task_create("SendUAVCAN", 20, 1024, send_app, NULL);
	if (ret < 0) {
		return ret;
>>>>>>> 8f8cb5e5201983f613807972ef9cc45a18d0a071
	}
	return 0;
}


/****************************************************************************
 * sensor_main
 *
 ****************************************************************************/
extern "C" {
#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int sensor_main(int status, char *name)
#endif
  {
<<<<<<< HEAD
	nsh_initialize();

	task_create("SENSOR_APP", 100, 2048, uavcan_task, NULL);
	return 0;

#if SOLO

	// Initialize microcontroller (Needed to initialize drivers)
	//nsh_initialize();


	// Test Sending

=======
	/*
	 * Open message queue, create semaphore and initialize  and start uavcan
	 * node.
	 */
	if (name != NULL && status == UAVCAN_START && !started) {
		sem = sem_open("/uavcan", O_CREAT, 0664 , 1);
		queue_name = name;
		queue = mq_open(queue_name, O_RDONLY);
		start_task();
		return 0;
	}
>>>>>>> 8f8cb5e5201983f613807972ef9cc45a18d0a071

	/*
	 * Close message queue, close semaphore and stop and delete uavcan
	 * node.
	 */
	if (status == UAVCAN_STOP && started) {
		stop_app();
		mq_unlink(queue_name);
		sem_close(sem);
		return 0;
	}

	/*
	 * Send uavan message.
	 */
	if (status == UAVCAN_SEND) {
		send_task();
		return 0;
	}
	return -1;
  }
}
