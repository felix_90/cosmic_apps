/****************************************************************************
 * examples/sensor/lib/driver/CanDriver.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_DRIVER_CANDRIVER_H_
#define APPS_EXAMPLES_SENSOR_LIB_DRIVER_CANDRIVER_H_
#include <sys/ioctl.h>

#include <fcntl.h>
#include <sys/types.h>
#include <nuttx/can/can.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "../../../sensor/lib/uavcan/UavcanMsg.h"

namespace uavcan {

class CanDriver {
  CanDriver();
  typedef void (*fptr)(uavcan_msg_s *msg);
  static CanDriver *can_driver;
  struct callbackList_s {
    callbackList_s()
        : function(NULL), dataTypeId(0), next(NULL)
      {
      }
    fptr function;
    uint16_t dataTypeId;
    callbackList_s *next;
  };
  int fd;
  callbackList_s *callbackList;
  callbackList_s* getCallbackFunction(uint16_t dataTypeId);
  bool isOpen();
  uint8_t getHex(char c);

public:
  static CanDriver* getDriver();
  int open_uavcan();
  void print(bool detail, bool read, uavcan_msg_s *msg);
  void close_uavcan();
  int restart_uavcan();
  int write_uavcan(uavcan_msg_s *msg);
  int read_uavcan();
  void setCallbackFunction(uint16_t dataTypeId, fptr function);
  void removeCallbackFunction(uint16_t dataTypeId);
  virtual ~CanDriver();
};

} /* namespace uavcan */

#endif /* APPS_EXAMPLES_SENSOR_LIB_DRIVER_CANDRIVER_H_ */
