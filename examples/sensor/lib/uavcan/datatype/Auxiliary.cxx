/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Auxiliary.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../../../../sensor/lib/uavcan/datatype/Auxiliary.h"

namespace uavcan {

Auxiliary::Auxiliary(gnss_aux_s &aux_data)
    : Data(16, SIGNATURE, SERVICE, DATATYPEID)
  {
    setGDOP(aux_data.gdop);
    setPDOP(aux_data.pdop);
    setHDOP(aux_data.hdop);
    setVDOP(aux_data.vdop);
    setTDOP(aux_data.tdop);
    setNDOP(aux_data.ndop);
    setEDOP(aux_data.edop);
    setSatsVisible(aux_data.sats_visible);
    setSatsUsed(aux_data.sats_used);
  }

void Auxiliary::setGDOP(float gdop)
  {
    uint16_t gdop16 = Data::getFloat16(gdop);
    Data::setData((void*) &gdop16, 0, 16);
  }

void Auxiliary::setPDOP(float pdop)
  {
    uint16_t pdop16 = Data::getFloat16(pdop);
    Data::setData((void*) &pdop16, 16, 16);
  }

void Auxiliary::setHDOP(float hdop)
  {
    uint16_t hdop16 = Data::getFloat16(hdop);
    Data::setData((void*) &hdop16, 32, 16);
  }

void Auxiliary::setVDOP(float vdop)
  {
    uint16_t vdop16 = Data::getFloat16(vdop);
    Data::setData((void*) &vdop16, 48, 16);
  }

void Auxiliary::setTDOP(float tdop)
  {
    uint16_t tdop16 = Data::getFloat16(tdop);
    Data::setData((void*) &tdop16, 64, 16);
  }

void Auxiliary::setNDOP(float ndop)
  {
    uint16_t ndop16 = Data::getFloat16(ndop);
    Data::setData((void*) &ndop16, 80, 16);
  }

void Auxiliary::setEDOP(float edop)
  {
    uint16_t edop16 = Data::getFloat16(edop);
    Data::setData((void*) &edop16, 96, 16);
  }

void Auxiliary::setSatsVisible(uint8_t sats_visible)
  {
    Data::setData((void*) &sats_visible, 112, 7);
  }

void Auxiliary::setSatsUsed(uint8_t sats_used)
  {
    Data::setData((void*) &sats_used, 119, 6);
  }

Auxiliary::~Auxiliary()
  {
//	printf("Delete MagnetFieldObject!\n");
  }

} /* namespace uavcan */
