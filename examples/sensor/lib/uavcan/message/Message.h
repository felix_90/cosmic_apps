/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Message.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_UAVCAN_MESSAGE_MESSAGE_H_
#define APPS_EXAMPLES_SENSOR_LIB_UAVCAN_MESSAGE_MESSAGE_H_

#include "../datatype/Data.h"
#include <crc16.h>
#include <string.h>
#include "../../driver/CanDriver.h"
#include "../UavcanMsg.h"

namespace uavcan {

class Message {
  uavcan_msg_s msg;
  uint16_t crc;
public:
  template<class T>
  Message(T *data, uint8_t transId, uint8_t prio, uint8_t nodeId)
    {
      //Set the Id field of uavcan_msg_s
      msg.can_id.msg_type = data->getDatatypeId();
      msg.can_id.node_id = nodeId;
      msg.can_id.priority = prio;
      msg.can_id.service = data->isService();

      uint8_t payloadSize = data->getSize();
      uint8_t* payloadData = data->getData();
      uint8_t msgCount;

//      printf("{");
//      for (int i = 0; i < payloadSize; i++) {
//    	  if (i % 10 == 0) {
//    		  printf("\n");
//    	  }
//    	  printf(" 0x%02x, ", payloadData[i]);
//      }
//      printf("};\n");

      //If the data does not fit in one message, calculate CRC
      if (payloadSize > 7)
        {
          uint64_t signature = data->getSignature();
          crc = crc16part((uint8_t*) &signature, 8, 0xffff);
          crc = crc16part(payloadData, payloadSize, crc);
          msgCount = (payloadSize + 1) / 7 + 1;		//Number of Messages
        }
      else
        {
          crc = 0;
          msgCount = 1;
        }
      //Calculate, how many messages are needed,
      // and set the msgCount of uavcan_msg_s

      msg.msgCount = msgCount;

      //Get the raw data of the datatype
      uavcan_payload_s payload[msgCount];

      //Set the data and tailByte for each Message
      uint8_t msgPos = 0;
      for (uint8_t payloadPos = 0; msgPos < msgCount; msgPos++)
        {
          payload[msgPos].tailByte.transfer_id = transId;

          //SingleFrame Message
          if (msgCount == 1)
            {
              payload[msgPos].tailByte.start = true;
              payload[msgPos].tailByte.end = true;
              payload[msgPos].tailByte.toggle = false;
			  memcpy((void*) payload[msgPos].data, (void*) payloadData, payloadSize);
			  payload[msgPos].size = payloadSize;
            }
          else
        	  //MultiFrame Message
            {
        	  	  if (msgPos == 0)			//First Message
        	  	    {
				  payload[msgPos].tailByte.start = true;
				  payload[msgPos].tailByte.end = false;
				  payload[msgPos].tailByte.toggle = false;
				  memcpy((void*) payload[msgPos].data, (void*) &crc, 2);
				  memcpy((void*) (payload[msgPos].data + 2), (void*) payloadData, 5);
				  payloadPos += 5;
				  payload[msgPos].size = 7;
        	  	    }
        	  	  else
        	  	    {
        	  		  if (msgPos+1 == msgCount)	//Last Message
        	  		    {
        				  payload[msgPos].tailByte.start = false;
        				  payload[msgPos].tailByte.end = true;
        				  payload[msgPos].tailByte.toggle = !payload[msgPos-1].tailByte.toggle;
        				  memcpy((void*) payload[msgPos].data, (void*) (payloadData + payloadPos), payloadSize - payloadPos);
        				  payload[msgPos].size = payloadSize - payloadPos;
        	  		    }
        	  		  else						//Intermediate Message
        	  		    {
        				  payload[msgPos].tailByte.start = false;
        				  payload[msgPos].tailByte.end = false;
        				  payload[msgPos].tailByte.toggle = !payload[msgPos-1].tailByte.toggle;
        				  memcpy((void*) payload[msgPos].data, (void*) (payloadData + payloadPos), 7);
        				  payloadPos += 7;
        				  payload[msgPos].size = 7;
        	  		    }
        	  	    }
            }
        }

      //Set the payload of uavcan_msg_s
      msg.payload = (uavcan_payload_s *) calloc(msgCount,
          sizeof(uavcan_payload_s));
      memcpy((void*) msg.payload, (void*) payload,
          sizeof(uavcan_payload_s) * msgCount);

    }

  int send();
  void print(bool detail);
  uint16_t getMessageType();
  virtual ~Message();
};

} //namespace uavcan
#endif /* APPS_EXAMPLES_SENSOR_LIB_UAVCAN_MESSAGE_MESSAGE_H_ */
